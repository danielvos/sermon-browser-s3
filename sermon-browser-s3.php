<?php /* -*- indent-tabs-mode: nil; -*- */
/*
 * Copyright 2011 Nathan Phillip Brink <ohnobinki@ohnopublishing.net>
 *
 * This file is part of sermon-browser-s3.
 *
 * sermon-browser-s3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sermon-browser-s3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with sermon-browser-s3.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Plugin Name: Sermon Browser S3
 * Plugin URI: http://ohnopub.net/w/Sermon_Browser_S3
 * Description: Automatic sermon backups using Amazon's S3 service.
 * Version: 0.3_pre
 * Author: Nathan Phillip Brink (binki)
 * Author URI: http://ohnopub.net/~ohnobinki/
 * License: AGPLv3
 * Provides: sermon-browser-s3
 */

/**
 * \brief
 *   Determine if the admin page should be accessible from the current
 *   context.
 */
function sermon_browser_s3_admin_access()
{
  /*
   * To edit the network-level options, you need manage_sites
   * privilige.
   */
  $capability = 'manage_options';
  if (is_network_admin())
    $capability = 'manage_sites';
  if (!current_user_can($capability))
    return FALSE;

  /*
   * If configuring sermon_browser_s3 per-site is prohibited, deny
   * access outside of the network administrator pages.
   */
  if (sermon_browser_s3_get_sitewide_override()
      && !is_network_admin())
    return FALSE;

  return TRUE;
}

/**
 * \brief
 *   Display configuration page for `S3 Backups'.
 */
function sermon_browser_s3_admin_page()
{
  if (!sermon_browser_s3_admin_access())
    wp_die(__('You do not have permissions to configure S3 Backups'));

  if (is_network_admin())
    {
      $get_option = 'get_site_option';
      $update_option = 'update_site_option';
      $delete_option = 'delete_site_option';
    }
  else
    {
      $get_option = 'get_option';
      $update_option = 'update_option';
      $delete_option = 'delete_option';
    }

  $aws_access_key = $get_option('sermon_browser_s3_aws_access_key', '');
  $aws_secret_key = $get_option('sermon_browser_s3_aws_secret_key', '');
  $s3_bucket = $get_option('sermon_browser_s3_bucket', '');
  $s3_path = $get_option('sermon_browser_s3_path', '');
  $delete = $get_option('sermon_browser_s3_delete', sermon_browser_s3_option_default('sermon_browser_s3_delete') == 'delete');
  if (is_network_admin())
    $cron_schedule = sermon_browser_s3_get_cron_schedule();
  else
    /* Default to `sitewide' */
    $cron_schedule = $get_option('sermon_browser_s3_cron_schedule', -1);

  echo '<div class="wrap">' . PHP_EOL;
  echo '<h2>' . htmlentities(__('Sermon Browser S3 Backups')) . '</h2>' . PHP_EOL;

  if (defined('MULTISITE') && !is_network_admin())
    {
      echo '<p>'
        . __('If any options are not specified below, the site-wide defaults will be used in their place.')
        . '</p>' . PHP_EOL;
    }

  /* Basic environment sanity check: */
  $s3 = sermon_browser_s3(TRUE);
  if (empty($s3))
    {
      echo '<div class="error"><p>'
        . __('Unable to find S3 class.'
             . ' Make sure to install <a href="http://undesigned.org.za/2007/10/22/amazon-s3-php-class">S3</a> into PHP\'s searchpath, such as into <tt>/usr/share/php</tt>.')
        . '</p></div>';
    }

  if (!empty($_POST['sermon_browser_s3_backup_now']))
    {
      /* Administrator requested immediate backups */
      if (is_network_admin())
        foreach (sermon_browser_s3_get_blog_ids() as $blog_id)
          {
            if (!switch_to_blog($blog_id, TRUE))
              continue;
            if (sermon_browser_s3_backup())
              $ret =  1;
            restore_current_blog();
          }
      else
        $ret = sermon_browser_s3_backup();

      if ($ret)
        echo '<div class="error"><p>'
          . __('There were problems performing the backup. Please see <tt>error_log</tt>.')
          . '</p></div>';
      else
        echo '<div class="updated"><p>'
          . __('Performing the backup was successul.')
          . '</p></div>';
    }
  elseif (!empty($_POST['sermon_browser_s3_admin_reset']))
    {
      $delete_option('sermon_browser_s3_aws_access_key');
      $delete_option('sermon_browser_s3_aws_secret_key');
      $delete_option('sermon_browser_s3_bucket');
      $delete_option('sermon_browser_s3_path');
      $delete_option('sermon_browser_s3_delete');
      $delete_option('sermon_browser_s3_cron_schedule');
      if (is_network_admin())
        $delete_option('sermon_browser_s3_sitewide_override');
      do_action('sermon_browser_s3_config_reset');

      do_action('sermon_browser_s3_config_updated');
    }
  elseif (isset($_POST['sermon_browser_s3_admin_submitted']))
    {
      /* Admninistrator submitted form */

      $old_s3_bucket = sermon_browser_s3_get_bucket();
      $old_delete = $delete;

      $aws_access_key = isset($_POST['sermon_browser_s3_aws_access_key']) ? $_POST['sermon_browser_s3_aws_access_key'] : $aws_access_key;
      $aws_secret_key = isset($_POST['sermon_browser_s3_aws_secret_key']) ? $_POST['sermon_browser_s3_aws_secret_key'] : $aws_secret_key;
      $s3_bucket = isset($_POST['sermon_browser_s3_bucket']) ? $_POST['sermon_browser_s3_bucket'] : $s3_bucket;
      $s3_path = trim(isset($_POST['sermon_browser_s3_path']) ? $_POST['sermon_browser_s3_path'] : $s3_path, '/');
      $delete = empty($_POST['sermon_browser_s3_submitting_delete']) ? $delete : !empty($_POST['sermon_browser_s3_delete']);
      $cron_schedule = isset($_POST['sermon_browser_s3_cron_schedule']) ? $_POST['sermon_browser_s3_cron_schedule'] : $cron_schedule;

      if (empty($aws_access_key))
        $delete_option('sermon_browser_s3_aws_access_key');
      else
        $update_option('sermon_browser_s3_aws_access_key', $aws_access_key);

      if (empty($aws_secret_key))
        $delete_option('sermon_browser_s3_aws_secret_key');
      else
        $update_option('sermon_browser_s3_aws_secret_key', $aws_secret_key);

      if (empty($s3_bucket))
        $delete_option('sermon_browser_s3_bucket');
      else
        $update_option('sermon_browser_s3_bucket', $s3_bucket);

      if (empty($s3_path))
        $delete_option('sermon_browser_s3_path');
      else
        $update_option('sermon_browser_s3_path', $s3_path);

      $update_option('sermon_browser_s3_delete', $delete ? 'delete' : 'no');

      if ($cron_schedule == -1)
        $delete_option('sermon_browser_s3_cron_schedule');
      else
        $update_option('sermon_browser_s3_cron_schedule', $cron_schedule);

      if (is_network_admin())
        {
          $sitewide_override = empty($_POST['sermon_browser_s3_submitting_sitewide_override'])
            ? $get_option('sermon_browser_s3_sitewide_override')
            : $_POST['sermon_browser_s3_sitewide_override'];
          $update_option('sermon_browser_s3_sitewide_override', $sitewide_override);
        }
      do_action('sermon_browser_s3_config_submitted');

      do_action('sermon_browser_s3_config_updated');

      if (!empty($s3_path))
        $example_s3_path = __(' The following is an example of what S3 Bucket Path will be expanded to: ') . '<code>' . htmlentities(strftime($s3_path)) . '</code>.';
      else
        $example_s3_path = '';
      echo '<div class="updated"><p>'
        . htmlentities(__('Updated S3 Backup settings.'))
        . $example_s3_path
        . '</p></div>' . PHP_EOL;

      $buckets = FALSE;
      $s3_bucket = sermon_browser_s3_get_bucket();
      /*
       * Reload the S3 object since the AWS Access/Secret Key may have
       * been updated.
       */
      $s3 = sermon_browser_s3(TRUE);
      if (!empty($s3))
        $buckets = $s3->listBuckets();
      if ($buckets === FALSE)
        {
          echo '<div class="error"><p>'
            . htmlentities(__('Unable to access Amazon S3 using the provided credentials, please correct this error.'))
            . '</p></div>' . PHP_EOL;
        }
      else
        {
          if ($old_s3_bucket != $s3_bucket
              && in_array($old_s3_bucket, $buckets))
            {
              /*
               * Try to delete/clean old bucket -- will only succeed if
               * it's empty.
               */
              if ($s3->deleteBucket($old_s3_bucket))
                {
                  echo '<div class="updated"><p>'
                    . htmlentities(__('Able to delete old bucket'))
                    . ' (<tt>' . htmlentities($old_s3_bucket) . '</tt>)'
                    . '</p></div>';
                }
              else
                {
                  echo '<div class="error"><p>'
                    . __('Unable to delete old bucket. Perhaps it still has sermons or other content stored in it. It will remain orphaned unless if you take further action. Note that Sermon Browser S3 will <em>not</em> copy sermons from the old bucket to the new one for you&mdash;if you change buckets, new sermons will be placed in the new bucket while old ones will be accessed through the old bucket.')
                    . ' (<tt>' . htmlentities($old_s3_bucket) . '</tt>)'
                    . '</p></div>';
                }
            }
          if (!in_array($s3_bucket, $buckets))
            {
              /*
               * Try to create/claim the user's requested bucket.
               */
              if ($s3->putBucket($s3_bucket, S3::ACL_PUBLIC_READ))
                {
                  echo '<div class="updated"><p>'
                    . __('Successfully created bucket')
                    . ' (<tt>' . htmlentities($s3_bucket) . '</tt>)'
                    . '</p></div>';
                }
              else
                {
                  echo '<div class="error"><p>'
                    . __('Unable to create bucket. Try choosing a different bucket name, since buckets must be globally unique.')
                    . ' (<tt>' . htmlentities($s3_bucket) . '</tt>)'
                    . '</p></div>';
                }
            }
        }
    }

  /*
   * Where's drupal's form_api hiding when it's most needed?
   */
  $will_be_deleted = '';
  if (defined('MULTISITE') && !is_network_admin())
    {
      if (sermon_browser_s3_get_delete())
        $will_be_deleted = __('Currently, sermons <em>will</em> be deleted after being uploaded to Amazon S3.');
      else
        $will_be_deleted = __('Currently, sermons <em>will not</em> be deleted after being uploaded to Amazon S3.');
      if ($delete != sermon_browser_s3_get_delete())
        $will_be_deleted = '<b>' . $will_be_deleted . '</b>';
      $will_be_deleted = ' ' . $will_be_deleted;
    }

  $option_info = array(
    'aws_access_key' => array(
      'opt' => 'aws_access_key',
      'name' => __('AWS Access Key'),
      'type' => 'text',
      'autocomplete' => FALSE,
      ),
    'aws_secret_key' => array(
      'opt' => 'aws_secret_key',
      'name' => __('AWS Secret Key'),
      'type' => 'password',
      'autocomplete' => FALSE,
      ),
    's3_bucket' => array(
      'opt' => 'bucket',
      'name' => __('S3 Bucket'),
      'type' => 'text',
      'description' => __('The bucket in which sermons will be stored.'),
      ),
    's3_path' => array(
      'opt' => 'path',
      'name' => __('S3 Bucket Path'),
      'type' => 'text',
      'description' => __('The path inside of the bucket to store sermons in. Leave blank to deposite sermons directly into the bucket. You may use any escapes supported by <a href="http://docs.php.net/strftime"><code>strftime()</code></a> (<code>%Y</code>: year, <code>%m</code>: month, <code>%d</code>: day, <code>%%</code>: literal &lsquo;%&rsquo;) to file the sermon based on the date it was preached.'),
      ),
    'delete' => array(
      'opt' => 'delete',
      'name' => __('Delete'),
      'type' => 'checkbox',
      'description' => __('Delete sermons from the wp-content folder after successfully uploading to S3.'
                          . $will_be_deleted),
      ),
    'cron_schedule' => array(
      'opt' => 'cron_schedule',
      'name' => __('Cron Frequency'),
      'type' => 'options',
      'options' => sermon_browser_s3_cron_schedule_options(),
      'description' => __('How often to perform the task of uploading local sermons to S3.'
                          . ' Sermons are not uploaded to S3 directly when they are uploaded to WordPress&mdash;'
                          . 'they are uploaded in the background when cron tasks are run.'),
      ),
    );

  if (is_network_admin())
    {
      $sitewide_override = $get_option('sermon_browser_s3_sitewide_override');
      $option_info['sitewide_override'] = array(
        'opt' => 'sitewide_override',
        'name' => __('Sitewide Override'),
        'type' => 'checkbox',
        'description' => __('Disable configuring Sermon Browser S3 per individual site.'
                            .' Only let this administrative interface be used to control it.'),
        );
    }

  /* drupal-like hook_FORM_ID_alter(). */
  $option_info = apply_filters('sermon_browser_s3_admin_form_alter',
                               $option_info);

  echo '<form name="sermon_browser_s3_admin" class="sermon-browser-s3-admin-form" method="post">' . PHP_EOL;
  echo '  <input name="sermon_browser_s3_admin_submitted" type="hidden" value="1"/>' . PHP_EOL;
  echo '  <table>' . PHP_EOL;
  foreach ($option_info as $varname => $the_option_info)
    {
      $opt_dashed = str_replace('_', '-', $the_option_info['opt']);

      $autocomplete = '';
      if (isset($the_option_info['autocomplete']))
        $autocomplete = ' autocomplete="' . ($the_option_info['autocomplete'] ? 'on' : 'off') . '"';

      echo ''
        . '  <tr class="form-table sermon-browser-s3-admin-' . $opt_dashed . '-wrapper">' . PHP_EOL
        . '    <th scope="row">' . PHP_EOL
        . '    <label for="sermon-browser-s3-' . $opt_dashed . '">' . PHP_EOL
        . '      ' . htmlentities($the_option_info['name']) . PHP_EOL
        . '    </label>' . PHP_EOL
        . '    </th>' . PHP_EOL
        . '    <td>' . PHP_EOL;

      $checked = '';
      $value = htmlentities(${$varname}, ENT_QUOTES);

      switch ($the_option_info['type'])
        {
        case 'password':
        case 'text':
          $class = 'regular-text code';
          break;

        case 'checkbox':
          echo '    <input type="hidden" name="sermon_browser_s3_submitting_' . $the_option_info['opt'] . '" value="1"/>' . PHP_EOL;

          if (${$varname})
            $checked = ' checked="checked"';
          $value = '1';
          break;

        default:
          $class = '';
        }

      switch ($the_option_info['type'])
        {
        case 'options':
          echo ''
            . '    <select name="sermon_browser_s3_' . $the_option_info['opt'] . '"'
            . '      id="sermon-browser-s3-' . $opt_dashed . '">' . PHP_EOL;
          foreach ($the_option_info['options'] as $value => $option)
            {
              $selected = $value == ${$varname} ? ' selected="selected"' : '';
              echo '      <option value="' . htmlentities($value, ENT_QUOTES) . '"' . $selected . '>'
                . $option . '</option>' . PHP_EOL;
            }
          echo ''
            . '    </select>' . PHP_EOL;
          break;

        default:
          echo ''
            . '    <input name="sermon_browser_s3_' . $the_option_info['opt'] . '"' . PHP_EOL
            . '      id="sermon-browser-s3-' . $opt_dashed . '"' . PHP_EOL
            . '      type="' . $the_option_info['type'] . '"' . PHP_EOL
            . '      value="' . htmlentities($value, ENT_QUOTES) . '"'
            . '      class="' . htmlentities($class, ENT_QUOTES) . '"' . $checked . $autocomplete . '/>' . PHP_EOL;
          break;
        }
      if (!empty($the_option_info['description']))
        echo '    <span class="sermon-browser-s3-admin-option-description description">'
          . $the_option_info['description'] . '</span>' . PHP_EOL;
      echo '    </td>' . PHP_EOL;
      echo '  </tr>' . PHP_EOL;
    }
  echo '  </table>' . PHP_EOL;
  echo '  <div><input type="submit" value="' . htmlentities(__('Update'), ENT_QUOTES) . '"/>' . PHP_EOL;
  echo '  <input name="sermon_browser_s3_admin_reset" type="submit" value="' . htmlentities(__('Reset')) . '"/>' . PHP_EOL;
  echo '  <input name="sermon_browser_s3_backup_now" value="' . htmlentities(__('Backup Now'), ENT_QUOTES) . '" type="submit"/></div>' . PHP_EOL;
  echo '</form>' . PHP_EOL;
  echo '</div>';
}

/**
 * \brief
 *   Retrieve an optionset of available cron schedules.
 */
function sermon_browser_s3_cron_schedule_options()
{
  $cron_options = array();
  if (defined('MULTISITE') && !is_network_admin())
    $cron_options[-1] = 'Sitewide Default';
  $cron_options[''] = 'Never';

  foreach (wp_get_schedules() as $schedule_key => $schedule)
    $cron_options[$schedule_key] = $schedule['display'];
  return $cron_options;
}

/**
 * \brief
 *   Guess the possible slugs which sermon-browser might be using.
 */
function sermon_browser_s3_sb_slugs()
{
  /*
   * Hack to guess sermon.php's menu slug for if
   * http://sermonbrowser.com/forum/suggestions/fix-installation-into-folder-other-than-sermon-browser/
   * isn't fixed yet while staying compatible with the fixed version.
   */
  $sb_slugs = array('sermon-browser/sermon.php' => TRUE);
  $active_plugins = (array)get_option('active_plugins', array());
  foreach ($active_plugins as $plugin)
    if (strpos($plugin, 'sermon.php') !== FALSE)
      $sb_slugs[plugin_basename($plugin)] = TRUE;

  return $sb_slugs;
}

/**
 * \brief
 *   Add an `S3 Backups' submenu to Sermon Browser's administration
 *   menu.
 */
function sermon_browser_s3_admin_menu_action()
{
  if (!sermon_browser_s3_admin_access())
    return;

  foreach (sermon_browser_s3_sb_slugs() as $sb_slug => $foo)
    add_submenu_page($sb_slug, __('S3 Backups'), __('S3 Backups'), 'manage_options',
                     'sermon-browser-s3-options', 'sermon_browser_s3_admin_page');
}

/* Hook must be run after sermon.php's admin_menu hook */
add_action('admin_menu', 'sermon_browser_s3_admin_menu_action', 12);
add_action('network_admin_menu', 'sermon_browser_s3_admin_menu_action', 12);


function sermon_browser_s3_admin_bar_menu_action()
{
  global $wp_admin_bar;

  if (!sermon_browser_s3_admin_access())
    return;
  if (!current_user_can('edit_posts'))
    return;

  $wp_admin_bar->add_menu(array(
      'parent' => 'sermon-browser-menu',
      'id' => 'sermon-browser-s3-menu',
      'title' => __('Sermons S3'),
      'href' => admin_url('admin.php?page=sermon-browser-s3-options'),
      ));
}
add_action('admin_bar_menu', 'sermon_browser_s3_admin_bar_menu_action', 12);

/**
 * \brief
 *   A stub Network menu for sermon-browser.
 */
function sermon_browser_s3_sb_menu_stub()
{
  echo '<h2>' . __('Sermon Browser') . '</h2>'
    . '<p>' . __('Sermon Browser does not yet have a Network administration page, but <a href="'
                 . /* menu_page_url('sermon-browser-s3-options', FALSE) */ 'admin.php?page=sermon-browser-s3-options'
                 . '">Sermon Browser S3 Backups</a> does.') . '</p>';
}

/**
 * \brief
 *   Ensure that there is a Sermons tab in the Network administration
 *   interface.
 */
function sermon_browser_s3_network_admin_menu_action()
{
  if (!sermon_browser_s3_admin_access())
    return;

  /* bad hack */
  global $admin_page_hooks;

  $sb_slugs = sermon_browser_s3_sb_slugs();
  foreach ($sb_slugs as $sb_slug => $foo)
    if (empty($admin_page_hooks[$sb_slug]))
      $sb_slug = NULL;
  if (empty($sb_slug))
    {
      $sb_slug = 'sermon-browser/sermon.php';

      /* We need to add the Sermons Network menu ourselves */
      add_menu_page(__('Sermons'), __('Sermons'), 'publish_posts', $sb_slug, 'sermon_browser_s3_sb_menu_stub', SB_PLUGIN_URL.'/sb-includes/sb-icon.png');
    }
}
/* before sermon_browser_s3_admin_menu_action() */
add_action('network_admin_menu', 'sermon_browser_s3_network_admin_menu_action', 11);

/**
 * \brief
 *   Get an option for the specific blog, falling back on the
 *   site-wide option.
 *
 * For the site-wide administration menu, this function will only
 * access site-wide values. If sitewide-override is enabled, this
 * function will only retrieve sitewide values.
 *
 * \param $name
 *   The name of the option to retrieve.
 * \param $default
 *   The default value of the option if it is not available.
 * \return
 *   The value of the option or $default if it's not set.
 */
function sermon_browser_s3_get_option($name, $default = FALSE)
{
  if (func_num_args() < 2)
    $default = sermon_browser_s3_option_default($name);

  $default = get_site_option($name, $default);
  if (sermon_browser_s3_get_sitewide_override()
      || is_network_admin())
    return $default;

  return get_option($name, $default);
}

/**
 * \brief
 *   Retrieve the default value for an option.
 *
 * \param $name
 *   The option whose default value should be retrieved.
 * \param $saved
 *   Set to TRUE if a default value is known, FALSE otherwise.
 * \return
 *   The default or FALSE.
 */
function sermon_browser_s3_option_default($name, &$saved = NULL)
{
  static $defaults = array(
    'sermon_browser_s3_aws_access_key' => '',
    'sermon_browser_s3_aws_secret_key' => '',
    'sermon_browser_s3_bucket' => '',
    'sermon_browser_s3_delete' => 'no',
  );

  if (isset($defaults[$name]))
    {
      $saved = TRUE;
      return $defaults[$name];
    }
  $saved = FALSE;
  return FALSE;
}

/**
 * \brief
 *   Determine if sitewide override is in place.
 */
function sermon_browser_s3_get_sitewide_override()
{
  return defined('MULTISITE')
    && get_site_option('sermon_browser_s3_sitewide_override');
}

/**
 * \brief
 *   Retrieve the bucket used for S3 backups.
 */
function sermon_browser_s3_get_bucket()
{
  return sermon_browser_s3_get_option('sermon_browser_s3_bucket',
                                      sermon_browser_s3_option_default('sermon-browser_s3_bucket'));
}

/**
 * \brief
 *   Retrieve whether or not to delete sermons after transfering.
 */
function sermon_browser_s3_get_delete()
{
  return sermon_browser_s3_get_option('sermon_browser_s3_delete',
                                      sermon_browser_s3_option_default('sermon_browser_s3_delete'))
    == 'delete';
}

/**
 * \brief
 *   Retrieve the chosen cron job schedule of the cron job.
 *
 * \return
 *   The cron schedule (suitable for passing to wp_schedule_event())
 *   or NULL if crons are disabled.
 */
function sermon_browser_s3_get_cron_schedule()
{
  $schedules = wp_get_schedules();
  $cron_schedule = sermon_browser_s3_get_option('sermon_browser_s3_cron_schedule', -1);
  /*
   * Pass `NULL' through (for cron disabled). -1 means find default.
   */
  if (!empty($cron_schedule) && empty($schedules[$cron_schedule]))
    {
      reset($schedules);
      if (!empty($schedules['daily']))
        $cron_schedule = 'daily';
      else
        $cron_schedule = key($schedules);
    }
  return $cron_schedule;
}

/**
 * \brief
 *   Schedule cron tasks with wordpress.
 *
 * Clears out existing tasks and reconfigures them.
 */
function sermon_browser_s3_set_cron()
{
  wp_clear_scheduled_hook('sermon_browser_s3_cron');
  /* Enable disabling the cronjob, NULL means disabled: */
  $cron_schedule = sermon_browser_s3_get_cron_schedule();
  if (empty($cron_schedule))
    return;
  wp_schedule_event(time(), $cron_schedule, 'sermon_browser_s3_cron');
}
register_activation_hook(__FILE__, 'sermon_browser_s3_set_cron');

/**
 * \brief
 *   Schedule cron tasks for potentially multiple blogs.
 */
function sermon_browser_s3_set_crons()
{
  if (is_network_admin())
    foreach (sermon_browser_s3_get_blog_ids() as $blog_id)
      {
        if (!switch_to_blog($blog_id, TRUE))
          continue;
        sermon_browser_s3_set_cron();
        restore_current_blog();
      }
  else
    sermon_browser_s3_set_cron();
}
add_action('sermon_browser_s3_config_updated', 'sermon_browser_s3_set_crons');

/**
 * \brief
 *   Return an array of all blog IDs.
 */
function sermon_browser_s3_get_blog_ids()
{
  global $wpdb;

  return array_map('_sermon_browser_s3_get_blog_ids_map',
                   $wpdb->get_results('SELECT blog_id FROM ' . $wpdb->prefix . 'blogs'));
}

/**
 * \brief
 *   Helper for sermon_browser_s3_get_blog_ids().
 * \internal
 */
function _sermon_browser_s3_get_blog_ids_map($blog)
{
  return $blog->blog_id;
}

/**
 * \brief
 *   Unregister cron tasks registered with wordpress.
 */
function sermon_browser_s3_unset_cron()
{
  wp_clear_scheduled_hook('sermon_browser_s3_cron');
}
register_deactivation_hook(__FILE__, 'sermon_browser_s3_unset_cron');


/**
 * \brief
 *   Perform the backups of sermons to Amazon S3.
 */
function sermon_browser_s3_backup()
{
  global $wpdb;

  if (!sermon_browser_s3_lock_grab('backup', 300))
    {
      error_log('sermon-browser-s3: Cancelling backup run because it is locked.');
      return 1;
    }
  do_action('sermon_browser_s3_backup');

  $s3 = sermon_browser_s3();
  $s3_bucket = sermon_browser_s3_get_bucket();
  $delete = sermon_browser_s3_get_delete();

  $have_bucket = FALSE;
  $upload_dir = SB_ABSPATH . sb_get_option('upload_dir');

  $ret = 0;
  /*
   * Only grab files claimed by sermons, since the upload directory
   * could be pointing at a shared directory of files -- sermonbrowser
   * might not be the only app managing them.
   */
  $files = $wpdb->get_results("SELECT stuff.id, stuff.name, sermons.datetime"
      . " FROM {$wpdb->prefix}sb_stuff stuff"
      . " JOIN {$wpdb->prefix}sb_sermons sermons ON stuff.sermon_id = sermons.id"
      . " WHERE stuff.type = 'file';");
  foreach ($files as $file)
    {
      /* Make sure that long runs don't get killed */
      sermon_browser_s3_lock_refresh('backup', 300);

      $filename = $upload_dir . $file->name;
      if (!file_exists($filename))
        continue;

      if (empty($file->datetime)
          /* sermon-browser uses this test instead of setting the field to NULL: */
          || $file->datetime == '1970-01-01 00:00:00'
          || ($time = strtotime($file->datetime)) === FALSE)
        $time = time();
      $dir_path = strftime(sermon_browser_s3_get_option('sermon_browser_s3_path', ''), $time);
      if (strlen($dir_path))
        $dir_path .= '/';

      $need_put = TRUE;
      $uri_relative = $dir_path . rawurlencode($file->name);
      $i = 0;
      do
        {
          $need_new_uri = FALSE;

          $existing_object_info = $s3->getObjectInfo($s3_bucket, $uri_relative);
          if ($existing_object_info !== FALSE)
            {
              if (!strcmp($existing_object_info['hash'], md5_file($filename)))
                /* File has already been uploaded -- md5sum matches */
                $need_put = FALSE;
              else
                {
                  /*
                   * We need to come up with a different URI and then
                   * test if that exists.
                   */
                  $need_new_uri = TRUE;

                  $filename_parts = explode('.', $file->name);
                  $extension = array_pop($filename_parts);
                  $uri_relative = $dir_path
                    . rawurlencode(sprintf("%s-%d.%s",
                                           implode('.', $filename_parts), 
                                           ++ $i,
                                           $extension));
                }
            }
        }
      while ($need_new_uri);

      if ($need_put
          && !$s3->putObjectFile($filename, $s3_bucket, $uri_relative,
                                 S3::ACL_PUBLIC_READ))
        {
          error_log('sermon-browser-s3: Unable to upload file{id=' . $file->id . ', file=' . $filename . ', name=' . $uri_relative . '} to amazon S3.');

          /**
           * \todo
           *   Figure out the proper method to log failures, whatever
           *   is like drupal's watchdog().
           */
          $ret = 1;
          continue;
        }
      /* Successfully uploaded or already uploaded beforehand: */

      $uri = 'http://' . $s3_bucket . '.s3.amazonaws.com/' . $uri_relative;
      $wpdb->update($wpdb->prefix . 'sb_stuff',
                    array('type' => 'url',
                          'name' => $uri),
                    array('id' => $file->id),
                    '%s', '%d');
      $wpdb->insert($wpdb->prefix . 'sermon_browser_s3',
                    array('filename' => $file->name,
                          'uri' => $uri),
                    '%s');

      if ($delete)
        {
          @unlink($filename);
          /* Support Windows */
          while (file_exists($filename))
            {
              sermon_browser_s3_lock_refresh('backup', 30);
              sleep(1);
              unlink($filename);
            }
        }

      sermon_browser_s3_lock_refresh('backup', 300);
      do_action('sermon_browser_s3_uploaded', $file->id, $uri_relative, $uri);
    }

  sermon_browser_s3_lock_release('backup');
  return $ret;
}
add_action('sermon_browser_s3_cron', 'sermon_browser_s3_backup');

/**
 * \brief
 *   Grab a lock and update script lifetime.
 *
 * Small locking API for serializing sermon_browser_backup()'s
 * operation.
 *
 * \param $name
 *   The name of the lock to grab.
 * \param $lifetime
 *   The lifetime of the lock in seconds.
 * \return
 *   TRUE if the lock could be grabbed, FALSE otherwise.
 */
function sermon_browser_s3_lock_grab($name, $lifetime)
{
  $r = rand();
  set_transient('sermon_browser_s3_lock_grab_' . $name, $r, 16);
  sleep(2);
  if ($r !== get_transient('sermon_browser_s3_lock_grab_' . $name))
    return FALSE;

  if (get_transient('sermon_browser_s3_lock_' . $name) !== FALSE)
    return FALSE;

  set_transient('sermon_browser_s3_lock_' . $name, TRUE, $lifetime + 16);
  set_time_limit($lifetime);
  return TRUE;
}

/**
 * \brief
 *   Refresh a lock and update script's lifetime.
 *
 * \param $name
 *   The name of the lock.
 * \param $lifetime
 *   The number of seconds the lock is needed.
 */
function sermon_browser_s3_lock_refresh($name, $lifetime)
{
  set_transient('sermon_browser_s3_lock_' . $name, TRUE, $lifetime + 16);
  set_time_limit($lifetime);
}

/**
 * \brief
 *   Release a lock.
 *
 * \param $name
 *   The name of the lock to release.
 */
function sermon_browser_s3_lock_release($name)
{
  delete_transient('sermon_browser_s3_lock_' . $name);
}

/**
 * \brief
 *   Instantiate an S3 object.
 *
 * \param $soft
 *   Try to make failures soft instead of hard.
 * \return
 *   NULL if S3 class is not available.
 */
function sermon_browser_s3($soft = FALSE)
{
  if (!class_exists('S3'))
    if ($soft)
      {
        @include 'S3.php';
        if (!class_exists('S3'))
          return NULL;
      }
    else
      require 'S3.php';

  $s3 = new S3(sermon_browser_s3_get_option('sermon_browser_s3_aws_access_key'),
               sermon_browser_s3_get_option('sermon_browser_s3_aws_secret_key'));

  /*
   * Let other plugins set the credentials if they want. For example,
   * a plugin could remove the `access' and `secret' keys from the
   * admin page and pull them from another plugin.
   */
  return apply_filters('sermon_browser_s3', $s3);
}

/**
 * \brief
 *   Intercept sb_hijack() to provide redirects to downloads as
 *   appropriate.
 */
function sermon_browser_s3_hijack()
{
  global $wpdb;

  if ((isset($_GET['download']) || isset($_GET['show']))
      && !empty($_GET['file_name']))
    {
      $file_name = rawurldecode($_GET['file_name']);
      $uri = $wpdb->get_var('SELECT uri FROM ' . $wpdb->prefix . 'sermon_browser_s3'
                            . ' WHERE filename="' . $wpdb->escape($file_name) . '"');

      if (empty($uri))
        /*
         * This is probably something that sb_hijack() can handle
         * fine.
         */
        return;

      /* Enable plugins to opt out of our hijacking */
      if (!apply_filters('sermon_browser_s3_hijack', TRUE, $file_name, $uri, isset($_GET['download']) ? 'download' : 'show'))
        return;

      /*
       * Point to the show&url=<uri> so that the hitcounter gets
       * updated, etc.
       */

      /*
       * Hack: Get a version of sb_display_url() that works in an
       * unbootstrapped wordpress (SB_AJAX responses don't require
       * get_permalink()).
       */
      define('SB_AJAX', TRUE);
      $sb_uri = sb_display_url() . sb_query_char(FALSE)
        . (isset($_GET['download']) ? 'download' : 'show') . '&url=' . rawurlencode($uri);
      header('HTTP/1.1 301 Moved Permanently');
      header('Location: ' . $sb_uri);
      die();
    }
}

/* Must have priority over sb_hijack(). */
add_action('plugins_loaded', 'sermon_browser_s3_hijack', 9);

/**
 * \brief
 *   Initialize tables used by sermon-browser-s3.
 */
function sermon_browser_s3_install()
{
  global $wpdb;

  /*
   * When sermon-browser-s3 copies a sermon to an S3 store, it needs
   * to recall that it did so. This way, it can intercept requests
   * with `download&file_name=<file>' and redirect these references to
   * <file> to the new location.
   *
   * Perhaps this should key on sb_stuff.id and grab the URI from
   * sb_stuff.name instead of storing the URI here...
   */
  $table = 'CREATE TABLE ' . $wpdb->prefix . 'sermon_browser_s3 (
  filename TEXT NOT NULL,
  uri TEXT NOT NULL,
  PRIMARY KEY  (filename(32))
);';
  /* Recommended by http://codex.wordpress.org/Creating_Tables_with_Plugins */
  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta($table);
}
register_activation_hook(__FILE__, 'sermon_browser_s3_install');
