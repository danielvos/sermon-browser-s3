=== sermon-browser-s3 ===
Contributors: binki
Tags: amazon, s3, aws, sermons, backup
Requires at least: 3.1
Tested up to: 3.3
Stable tag: sermon-browser-s3-0.1

Extend sermon-browser to automatically move sermons from
wp-content/uploads to an Amazon S3 bucket.

== Description ==

sermon-browser-s3 will, when cron is run or when manually invoked,
take uploaded sermons registered with sermon-browser and post them to
an Amazon S3 bucket. After uploading a sermon, it will replace
reference to the sermon file with a reference to the Amazon S3 URI. It
then inserts a redirect record so that users following old links to
the sermon download will be properly forwarded to the new sermon
location. The sermon file will then, optionally, be deleted from the
local wp-content/uploads directory.

Hosting sermons with Amazon's S3 service moves the load of downloading
sermon files to Amazon's reliable service.

Given the supposed reliability of the S3 service and the optionally
automatic removal of sermons from the wp-content directory, this
plugin can help reduce the amount of data necessary to keep backed
up. A standard backup of wordpress's databases will preserve
references to sermons stored onto Amazon S3 using this plugin.

== Installation ==

This plugin has two external dependencies:

1. The
   [sermon-browser](http://wordpress.org/extend/plugins/sermon-browser)
   plugin. This must be installed and configured before
   sermon-browser-s3 will be of any use.

2. The [S3](http://undesigned.org.za/2007/10/22/amazon-s3-php-class)
   PHP API. The `S3` class must be defined in the PHP environment. If
   the `S3` class is not present, sermon-browser-s3 will try to load
   it using the standard method of `require 'S3.php'`. This generally
   means that S3.php can only be properly installed if it is placed in
   PHP's default [include_path](http://docs.php.net/include-path),
   such as in `/usr/share/php`. S3 will not be distributed with
   sermon-browser-s3 as its proper place is the system include_path
   and it is the package manager's job to place it there.

Install the plugin using the common installation procedure (plop its
folder into `wp-content/plugins`). Activate it, and then configure it
by going to the *S3 Backups* tab of the *Sermons* menu:

1. On this page, you will be notified if sermon-browser-s3 had trouble
   finding the `S3` library.

2. For sermon-browser-s3 to work, it needs AWS credentials. Currently,
   it supports only the AWS Access Key and AWS Secret Key
   authentication method. Enter these into the configuration form.

3. Enter a globally-unique name for your bucket.  The bucket should
   have a computer-friendly name (i.e., no spaces, mostly alphabetic).

4. Choose if sermons should be deleted from `wp-content/uploads` after
   they are successfully updated.

5. Choose a frequency for running the cronjob which attempts to upload
   sermons to the Amazon S3 service.

After entering the configuration, click *Update* to save
it. sermon-browser-s3 will test if your AWS Access/Secret Key pair
credentials work and print a warning if they are invalid. If the
credentials are valid, sermon-browser-s3 will attempt to create a
bucket with the given bucket name and print information about this
step. If this fails, it means that someone else already owns that
bucket and you must try to choose a more unique bucket name -- perhaps
by appending random digits to your bucket name or making it more
specific to your institution's name.

To immediately start moving existing sermons to Amazon S3, click
*Backup Now*. This option will move all files attached to sermons to
Amazon S3 and print a message about the success of this step. If there
are problems with your configuration, a negative message should be
printed after a short amount of time. If a long period passes and a
negative message is printed, there are probably only problems
uploading specific sermons.

== Frequently Asked Questions ==

= Where can I report bugs or find development information? =

Start at http://ohnopub.net/w/Sermon_Browser_S3.

= What is the gist of the Affero GPL-3? =

This license states that if you make any modifications to this plugin,
you must publish them. This license [is incompatible with
GPLv2](http://www.gnu.org/licenses/license-list.html#AGPLv3.0), and
thus this plugin [may not be submitted to
wordpress.org](http://wordpress.org/extend/plugins/about/).

== Changelog ==

= sermon-browser-s3-0.1 =
* Add `Provides: sermon-browser-s3` so that other plugins can use
  `Depends: sermon-browser-s3` to support [Plugin
  Dependencies](http://wordpress.org/extend/plugins/plugin-dependencies/).
* Insert some hooks and filters, hopefully enabling extensibility by
  other plugins.
* Multisite support, including mixed multisite/per-site configuration
  and disabling per-site configuration entirely.
* Support for deleting each sermon after it has successfully been
  copied to S3.
* Cron, locking, and semi-transactional support for automatically
  uploading sermons to S3 in the background without interrupting
  access to sermons (with support for rewriting URLs which might be
  broken by the move).
* Initial plugin extending sermon-browser to copy sermons to [Amazon
  S3](http://aws.amazon.com/s3/) service.
* Chose Affero GPL-3 license.
